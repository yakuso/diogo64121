from app import app
from db_config import mysql
from flask import jsonify, render_template
from flask import request
import pymysql



@app.route('/')
def test():
    return ("Estás ligado")


# ----------------------------------------VER DADOS NA WEB----------------------------------------


@app.route('/reg')
def verregisto():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM registo")
    fetchdata = cursor.fetchall()
    cursor.close()
    return render_template('registo.html', output_data=fetchdata)


@app.route('/sal')
def versalas():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM salas")
    fetchdata = cursor.fetchall()
    return render_template('salas.html', output_data=fetchdata)


@app.route('/camp')
def vercampus():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM campus")
    fetchdata = cursor.fetchall()
    cursor.close()
    return render_template('campus.html', output_data=fetchdata)


@app.route('/uni')
def veruni():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM universidade")
    fetchdata = cursor.fetchall()
    cursor.close()
    return render_template('universidade.html', output_data=fetchdata)


@app.route('/sens')
def versensores():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM sensor")
    fetchdata = cursor.fetchall()
    cursor.close()
    return render_template('sensores.html', output_data=fetchdata)


# ----------------------------------------INSERT----------------------------------------


@app.route('/addsalas', methods=['POST'])
def add_sala():
    conn = mysql.connect()
    cursor = conn.cursor()
    try:
        _json = request.json
        _id_salas = _json['id_salas']
        _id_campus = _json['id_campus']
        _numero = _json['numero']

        if _id_salas and _id_campus and _numero and request.method == 'POST':
            sql = "INSERT INTO salas(id_salas, id_campus, numero) VALUES (%s, %s, %s)"
            data = (_id_salas, _id_campus, _numero)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Sala adicionada com sucesso!')
            resp.status_code = 200
            print('Sala', _id_salas, 'adicionada com sucesso!')
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/addsensor', methods=['POST'])
def add_sensor():
    conn = mysql.connect()
    cursor = conn.cursor()
    try:
        _json = request.json
        _id_sensor = _json['id_sensor']
        _nome = _json['nome']
        _descricao = _json['descricao']
        _id_salas = _json['id_salas']

        if _id_sensor and _nome and _descricao and _id_salas and request.method == 'POST':
            sql = "INSERT INTO sensor(id_sensor, nome, descricao, id_salas) VALUES (%s, %s, %s, %s)"
            data = (_id_sensor, _nome, _descricao, _id_salas)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Sensor adicionado com sucesso!')
            resp.status_code = 200
            print('Sensor', _id_sensor, 'adicionado com sucesso!')
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/addregisto', methods=['POST'])
def add_registo():
    conn = mysql.connect()
    cursor = conn.cursor()
    try:
        _json = request.json
        _id_registo = _json['id_registo']
        _valor = _json['valor']
        _data = _json['data']
        _sensor_id = _json['sensor_id']

        if _id_registo and _valor and _data and _sensor_id and request.method == 'POST':
            sql = "INSERT INTO registo(id_registo, valor, data, sensor_id) VALUES (%s, %s, %s, %s)"
            data = (_id_registo, _valor, _data, _sensor_id)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Registo adicionado com sucesso!')
            resp.status_code = 200
            print('Registo', _id_registo, 'adicionado com sucesso!')
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/addcampus', methods=['POST'])
def add_campus():
    conn = mysql.connect()
    cursor = conn.cursor()
    try:
        _json = request.json
        _id_campus = _json['id_campus']
        _id_universidade = _json['id_universidade']
        _nome = _json['nome']

        if _id_campus and _id_universidade and _nome and request.method == 'POST':
            sql = "INSERT INTO campus(id_campus, id_universidade, nome) VALUES (%s, %s, %s)"
            data = (_id_campus, _id_universidade, _nome)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Campus adicionado com sucesso!')
            resp.status_code = 200
            print('Campus', _id_campus, 'adicionado com sucesso!')
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/adduni', methods=['POST'])
def add_uni():
    conn = mysql.connect()
    cursor = conn.cursor()
    try:
        _json = request.json
        _id_universidade = _json['id_universidade']
        _nome = _json['nome']

        if _id_universidade and _nome and request.method == 'POST':
            sql = "INSERT INTO universidade(id_universidade, nome) VALUES (%s, %s)"
            data = (_id_universidade, _nome)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Universidade adicionada com sucesso!')
            resp.status_code = 200
            print('Universidade', _id_universidade, 'adicionada com sucesso!')
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


# ----------------------------------------SELECT----------------------------------------


@app.route('/salas')
def salas():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("SELECT * FROM salas")
        rows = cursor.fetchall()
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/campus')
def campus():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("SELECT * FROM campus")
        rows = cursor.fetchall()
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/sensores')
def sensores():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("SELECT * FROM sensor")
        rows = cursor.fetchall()
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/registo')
def registo():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("SELECT * FROM registo")
        rows = cursor.fetchall()
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/universidade')
def universidade():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("SELECT * FROM universidade")
        rows = cursor.fetchall()
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


# ----------------------------------------SELECT BY ID----------------------------------------


@app.route('/salas/<int:id>')
def salas_id(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:

        cursor.execute("SELECT * FROM salas WHERE id_salas=%s", id)
        row = cursor.fetchone()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/campus/<int:id>')
def campus_id(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:

        cursor.execute("SELECT * FROM campus WHERE id_campus=%s", id)
        row = cursor.fetchone()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/sensor/<int:id>')
def sensor_id(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:

        cursor.execute("SELECT * FROM sensor WHERE id_sensor=%s", id)
        row = cursor.fetchone()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/uni/<int:id>')
def uni_id(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:

        cursor.execute("SELECT * FROM universidade WHERE id_universidade=%s", id)
        row = cursor.fetchone()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/registo/<int:id>')
def registo_id(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:

        cursor.execute("SELECT * FROM registo WHERE id_registo=%s", id)
        row = cursor.fetchone()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


# ----------------------------------------UPDATE----------------------------------------


@app.route('/upsala', methods=['POST'])
def update_sala():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        _json = request.json
        _id_salas = _json['id_salas']
        _id_campus = _json['id_campus']
        _numero = _json['numero']
        if _id_salas and _id_campus and _numero and request.method == 'POST':
            sql = "UPDATE salas SET id_campus=%s, numero=%s WHERE id_salas=%s"
            data = (_id_campus, _numero, _id_salas)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Sala updated')
            resp.status_code = 200
            print("Sala com o ID:", _id_salas, "mudada com sucesso!")
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/upuni', methods=['POST'])
def update_uni():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        _json = request.json
        _id_universidade = _json['id_universidade']
        _nome = _json['nome']
        if _id_universidade and _nome and request.method == 'POST':
            sql = "UPDATE universidade SET nome=%s WHERE id_universidade=%s"
            data = (_nome, _id_universidade)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Universidade updated!')
            resp.status_code = 200
            print('Universidade', _id_universidade, 'mudada com sucesso!')
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/upcampus', methods=['POST'])
def update_campus():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        _json = request.json
        _id_campus = _json['id_campus']
        _id_universidade = _json['id_universidade']
        _nome = _json['nome']
        if _id_campus and _id_universidade and _nome and request.method == 'POST':
            sql = "UPDATE campus SET id_universidade=%s, nome=%s WHERE id_campus=%s"
            data = (_id_universidade, _nome, _id_campus)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Campus updated!')
            resp.status_code = 200
            print('Campus', _id_universidade, 'mudado com sucesso!')
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/upsensor', methods=['POST'])
def update_sensor():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        _json = request.json
        _id_sensor = _json['id_sensor']
        _nome = _json['nome']
        _descricao = _json['descricao']
        _id_salas = _json['id_salas']
        if _id_sensor and _nome and _descricao and _id_salas and request.method == 'POST':
            sql = "UPDATE sensor SET nome=%s, descricao=%s, id_salas=%s WHERE id_sensor=%s"
            data = (_nome, _descricao, _id_salas, _id_sensor)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Sensor updated!')
            resp.status_code = 200
            print('Sensor', _id_sensor, 'mudado com sucesso!')
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/upregisto', methods=['POST'])
def update_registo():
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        _json = request.json
        _id_registo = _json['id_registo']
        _valor = _json['valor']
        _data = _json['data']
        _sensor_id = _json['sensor_id']
        if _id_registo and _valor and _data and _sensor_id and request.method == 'POST':
            sql = "UPDATE registo SET valor=%s, data=%s, sensor_id=%s WHERE id_registo=%s"
            data = (_valor, _data, _sensor_id, _id_registo)
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('Registo updated!')
            resp.status_code = 200
            print('Registo', _id_registo, 'mudado com sucesso!')
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


# ----------------------------------------DELETE BY ID----------------------------------------


@app.route('/deluni/<int:id>')
def delete_uni(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("DELETE FROM universidade WHERE id_universidade=%s", id)
        conn.commit()
        resp = jsonify('Universidade apagada com sucesso!')
        resp.status_code = 200
        print('Universidade', id, 'apagada com sucesso!')
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/delcampus/<int:id>')
def delete_campus(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("DELETE FROM campus WHERE id_campus=%s", (id))
        conn.commit()
        resp = jsonify('Campus apagado com sucesso!')
        resp.status_code = 200
        print('Campus', id, 'apagado com sucesso!')
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/delsala/<int:id>')
def delete_sala(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("DELETE FROM salas WHERE id_salas=%s", id)
        conn.commit()
        resp = jsonify('Sala apagada com sucesso!')
        resp.status_code = 200
        print('Sala', id, 'apagada com sucesso!')
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/delsensor/<int:id>')
def delete_sensor(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("DELETE FROM sensor WHERE id_sensor=%s", id)
        conn.commit()
        resp = jsonify('Sensor apagado com sucesso!')
        resp.status_code = 200
        print('Sensor', id, 'apagado com sucesso!')
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/delregisto/<int:id>')
def delete_registo(id):
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute("DELETE FROM registo WHERE id_registo=%s", id)
        conn.commit()
        resp = jsonify('Registo apagado com sucesso!')
        resp.status_code = 200
        print('Registo', id, 'apagado com sucesso!')
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


# ----------------------------------------ERROR----------------------------------------


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp


if __name__ == "__main__":
    app.run()
